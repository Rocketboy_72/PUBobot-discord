STATUS_MESSAGE_FIELD_NAMES = ['**Pickups**', '**Matches**', '**Parties**']
STATUS_MESSAGE_KEYS = ['pickup_msg', 'matches_msg', 'party_msg']


class ChannelList(list):
    def __init__(self):
        super().__init__()
        self.from_same_guild = True

    def append(self, value):
        super().append(value)

        if self.__len__() > 0:
            self.from_same_guild = self.from_same_guild and next(self.__iter__()).guild == value.guild


class StatusMessage:

    def __init__(self, discord_msg=None):
        self.discord_msg = discord_msg
        self.channels = ChannelList()

    # {
    #     "fields": [
    #         {
    #             "value": "#local [no pickups]",
    #             "name": "Pickups",
    #             "inline": true
    #         },
    #         {
    #             "value": "#local 1v1 | waiting_ready",
    #             "name": "Matches",
    #             "inline": true
    #         }
    #     ],
    #     "type": "rich",
    #     "title": "Status:"
    # }
    def build_embed_dict(self, destination_channel):
        rv = {
            'fields':[
            ],
            "title": "Status:"
        }
        for idx, key in enumerate(STATUS_MESSAGE_KEYS):
            field_value = '\n'.join([
                "{} {}".format(self.get_channel_link(channel, destination_channel), channel.status_message_dict[key])
                for channel in self.channels if channel.cfg['public'] and channel.status_message_dict[key] != ''])
            if field_value != '':
                rv['fields'].append({
                    'value': field_value,
                    'name': STATUS_MESSAGE_FIELD_NAMES[idx],
                    'inline': True
                })
        return rv

    def get_channel_link(self, channel, destination_channel):
        if channel.channel.guild == destination_channel.guild:
            return channel.channel.mention
        else:
            return "{}[#{}](https://discord.com/channels/{}/{})".format(
                "{} ".format(channel.guild.name) if not self.channels.from_same_guild else "",
                channel.channel.name,
                channel.guild.id, channel.channel.id)


