import re
from modules.exceptions import WrongTimeFormatException
from arrow.locales import EnglishLocale


def parse_time_input(time_list):
    time_int = 0
    for i in time_list:  # convert given time to float
        try:
            num = int(i[:-1])  # the number part
            if i[-1] == 'd':
                time_int += num * 3600 * 24
            elif i[-1] == 'h':
                time_int += num * 3600
            elif i[-1] == 'm':
                time_int += num * 60
            elif i[-1] == 's':
                time_int += num
            else:
                raise ValueError
        except (IndexError, ValueError):
            raise WrongTimeFormatException(i)
    return time_int


def split_large_message(text, delimiter="\n", charlimit=1999):
    templist = text.split(delimiter)
    tempstr = ""
    result = []
    print(templist)
    for i in range(0, len(templist)):
        tempstr += templist[i]
        msglen = len(tempstr)
        if msglen >= charlimit:
            raise Exception("Text split failed!")
        elif i + 1 < len(templist):
            tempstr += delimiter
            if msglen + len(templist[i + 1]) >= charlimit - 2:
                result.append(tempstr)
                tempstr = ""
        else:
            result.append(tempstr)
    return result


ranks = {
    2000: "〈★〉",
    1950: "〈A+〉",
    1900: "〈A〉",
    1850: "〈A-〉",
    1800: "〈B+〉",
    1750: "〈B〉",
    1700: "〈B-〉",
    1650: "〈C+〉",
    1600: "〈C〉",
    1550: "〈C-〉",
    1500: "〈D+〉",
    1450: "〈D-〉",
    1400: "〈E+〉",
    1350: "〈E〉",
    1300: "〈E-〉",
    1200: "〈F+〉",
    1100: "〈F〉",
    1000: "〈F-〉",
    0: "〈G〉"}

custom_ranks = {
    2000: ["〈🍆〉", "Supreme Leader"],
    1900: ["〈🛐〉", "God"],
    1800: ["〈👑〉", "Legend"],
    1700: ["〈\N{fire}\N{fire}\N{fire})", "Elite 3"],
    1650: ["〈\N{fire}\N{fire})", "Elite 2"],
    1600: ["〈\N{fire})", "Elite 1"],
    1550: ["(\N{Gem Stone}+)", "Diamond 2"],
    1500: ["〈\N{Gem Stone}-〉", "Diamond 1"],
    1450: ["〈\N{Trident Emblem}+)", "Gold 2"],
    1400: ["〈\N{Trident Emblem}-〉", "Gold 1"],
    1350: ["〈🦾+)", "Silver 2"],
    1300: ["〈🦾-〉", "Silver 1"],
    1250: ["(\N{jack-o-lantern}+)", "Bronze 2"],
    1200: ["(\N{jack-o-lantern}-)", "Bronze 1"],
    1150: ["(🥔)", "Potat"],
    1100: ["(🍠)", "Sweet Potat"],
    1050: ["(🥔🍠🥔)", "Ultimate Potat"],
    1000: ["(🧟)", "Zombie"],
    0: ["(👶)", "Harmless"]
}

christmas_ranks = {
    2000: ["(🌟)", "Star"],
    1900: ["(🍾)", "Champagne"],
    1800: ["(🍰)", "Party Cake"],
    1700: ["(🦌🎅🦌)", "Ultimate Santa"],
    1650: ["(🎄🎅)", "Elite Santa"],
    1600: ["(🎅)", "Santa Claus"],
    1550: ["(⛄+)", "Snowman 2"],
    1500: ["(⛄-)", "Snowman 1"],
    1450: ["(🎁+)", "Present 2"],
    1400: ["(🎁-〉", "Present 1"],
    1350: ["(❄+)", "Snowflake 2"],
    1300: ["(❄-〉", "Snowflake 1"],
    1250: ["(🧨+)", "Cracker 2"],
    1200: ["(🧨-)", "Cracker 1"],
    1150: ["(🍬)", "Delicious Candy"],
    1100: ["(🍬🍬)", "Sour Candy"],
    1050: ["(🍬🍬🍬)", "Sticky Candy"],
    1000: ["(🧚)", "Angel"],
    0: ["(👼)", "Baby Angel"]
}

def rating_to_icon(rating, custom, long=""):
    if custom == "1":
        for i in sorted(custom_ranks.keys(), reverse=True):
            if rating >= i:
                return custom_ranks[i][1] if long else custom_ranks[i][0]
    elif custom == "2":
        for i in sorted(christmas_ranks.keys(), reverse=True):
            if rating >= i:
                return christmas_ranks[i][1] if long else christmas_ranks[i][0]
    else:
        for i in sorted(ranks.keys(), reverse=True):
            if rating >= i:
                return ranks[i]


def int_to_emoji(number):
    digit_emojis = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    return ''.join([':' + digit_emojis[int(digit)] + ':' for digit in str(number)])


def get_id_from_mention(mention: str):
    return int(re.match(r"^<@!?(\d+)>$", mention).group(1))


def member_list(players, separator='/') -> str:
    return separator.join(["`" + format_member_nick(player) + "`" for player in players])


def format_member_nick(member) -> str:
    return (member.nick or member.name).replace("`", "")


class EnLocale(EnglishLocale):
    names = ['_en']

    timeframes = {
        "now": "just now",
        "second": "1s",
        "seconds": "{0}s",
        "minute": "1m",
        "minutes": "{0}m",
        "hour": "1h",
        "hours": "{0}h",
        "day": "1d",
        "days": "{0}d",
        "week": "a week",
        "weeks": "{0} weeks",
        "month": "a month",
        "months": "{0} months",
        "year": "a year",
        "years": "{0} years",
    }
