import asyncio
from discord.ext import ipc
import client_config

ipc_client = ipc.Client(secret_key=client_config.IPC_SECRET) # secret_key must be the same as your server
# ipc_client = ipc.Client(secret_key="dfsfdfdsfdfad") # secret_key must be the same as your server

loop = asyncio.new_event_loop()

# ipc_node = loop.run await ipc_client.discover() # discover IPC Servers on your network

member_count = loop.run_until_complete(
    ipc_client.request("get_member_count", guild_id=363806372837457921)
) # get the member count of server with ID 12345678

print(member_count)

print(loop.run_until_complete(
    ipc_client.request("get_pickups")
))

print(loop.run_until_complete(
    ipc_client.request("pls")
))
# print(str(member_count)) # display member count
