import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    socket: {
      isConnected: false,
      message: '',
      reconnectError: false,
    },
    match: null,
    user_id: null,
    isLogged: false,
    loginFailed: false,
    pickups: [],
    channel_stats: [],
    leader_board: null
  },
  mutations:{
    SOCKET_ONOPEN (state, event)  {
      Vue.prototype.$socket = event.currentTarget
      state.socket.isConnected = true
    },
    /* eslint-disable no-unused-vars */
    SOCKET_ONCLOSE (state, event)  {
      state.socket.isConnected = false
      state.isLogged = false
      console.log("conn closed")
    },

    //TODO: display error
    SOCKET_ONERROR (state, event)  {
    },
    // mutations for reconnect methods
    SOCKET_RECONNECT(state, count) {
    },
    /* eslint-enable no-unused-vars */
    SOCKET_ONMESSAGE (state, message)  {
      state.socket.message = message
      if (message == 'not logged in' || message == 'invalid grant') {
        state.loginFailed = true
      } else if (message.event == 'match_change') {
        if (message.match.state == 'cancelled' || message.match.state == 'finished')
          state.match = null
        else
          state.match = message.match
      } else if (message.event == 'state') {
        state.pickups = message.pickups
        state.user_id = message.user_id
        state.isLogged = true
      } else if (message.event == 'pickup_change') {
        var pickup_idx = state.pickups.findIndex( pickup => pickup.channel_id == message.pickup.channel_id && pickup.name == message.pickup.name )
        if (~pickup_idx) state.pickups[pickup_idx].players = message.pickup.players
        else state.pickups.push(message.pickup)
      } else if (message.event == 'set_ready') {
        Vue.set(state.match, 'players_ready', message.match.players_ready)
      } else if (message.event == 'player_pick') {
        Vue.set(state.match, 'alpha_team', message.match.alpha_team)
        Vue.set(state.match, 'beta_team', message.match.beta_team)
        Vue.set(state.match, 'unpicked', message.match.unpicked)
        Vue.set(state.match, 'pick_step', message.match.pick_step)
      } else if (message.event == 'stats') {
        state.channel_stats = message.stats
      } else if (message.event == 'user_pickups') {
        var channel_idx = state.channel_stats.findIndex(channel => channel.channel_id == message.channel_id)
        if (~pickup_idx) {
          state.channel_stats[channel_idx].pickups = message.data
          state.channel_stats[channel_idx].index = message.index
        }
      } else if (message.event == 'leader_board') {
        state.leader_board = message
      }
    },
    SOCKET_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;
    }
  },
  actions: {
    sendMessage: function(context, message) {
      Vue.prototype.$socket.send(message)
    },
    send_ready(context, is_ready) {
      Vue.prototype.$socket.sendObj({'action':'ready', 'is_ready':is_ready})
    },
    send_add(context, pickup) {
      Vue.prototype.$socket.sendObj({'action':'add', 'channel_id': pickup.channel_id, 'pickup_name':pickup.name})
    },
    send_remove(context, pickup) {
      Vue.prototype.$socket.sendObj({'action':'remove', 'channel_id': pickup.channel_id, 'pickup_name':pickup.name})
    },
    get_user_pickups(context, arg) {
      Vue.prototype.$socket.sendObj({'action':'get_user_pickups', 'channel_id': arg.channel_id, 'index':arg.index})
    },
    get_leader_board(context, arg) {
      this.state.leader_board = null
      Vue.prototype.$socket.sendObj({'action':'get_leader_board', 'channel_id': arg.channel_id, 'index':arg.index})
    },
    send_pick(context, player) {
      Vue.prototype.$socket.sendObj({'action':'pick_player', 'player_id':player.id})
    }
  },
  getters: {
    isLogged: state => {
      return state.isLogged
    },
    loginFailed: state => {
      return state.loginFailed
    },
    isConnected: state => {
      return state.socket.isConnected
    },
    channel_stats: state => {
      return state.channel_stats
    }
  }
})