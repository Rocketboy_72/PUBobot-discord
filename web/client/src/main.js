import Vue from 'vue'
import App from './App.vue'
import router from './router';
import store from './store'
import VueNativeSock from 'vue-native-websocket'
import VModal from 'vue-js-modal'

Vue.config.productionTip = false

//TODO: separate store for demo?
Vue.use(VueNativeSock, `${process.env.VUE_APP_WS_URL}`, { store: store, format: 'json',
  reconnection: true, reconnectionDelay: 5000, connectManually: true })
Vue.use(VModal, {})

const vm = new Vue({
  router,
  render: h => h(App),
  store
}).$mount('#app')

// inspiration: https://github.com/djsmedes/gmtools/blob/9c83e5c0d99721c50cb7762d4f0cd10977bc32b7/_src/main.js
// connects to websocket
vm.$connect()

store.subscribe((mutation, state) => {
  if (mutation.type == "SOCKET_ONCLOSE" && state.loginFailed) {
    vm.$disconnect()
  }
});
