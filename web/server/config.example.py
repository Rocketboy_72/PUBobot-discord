class Config:
    SECRET_KEY = b"change_me"
    DISCORD_CLIENT_ID = "change_me"
    DISCORD_CLIENT_SECRET = "change_me"
    DISCORD_REDIRECT_URI = "http://localhost:5000/callback"
    FRONTEND_URL = "http://localhost:8080"
    REDIS_URL = "redis://localhost:6379/0"
    ADMIN_USER_ID = "change_me"  # discord user id as integer
    ADD_TIME_LIMIT = 3
    # SESSION_COOKIE_DOMAIN = "test.com"
    # PREFER_SECURE_URLS = False
