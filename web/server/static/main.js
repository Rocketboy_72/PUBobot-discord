var ws_protocol = (window.location.protocol == "https:") ? 'wss' : 'ws';
var ws = new WebSocket(ws_protocol + '://' + document.domain + ':' + location.port + '/ws');

document.addEventListener('DOMContentLoaded', function() {
//    var es = new EventSource('/sse');
    ws.onmessage = function (event) {
        var messages_dom = document.getElementsByTagName('ul')[0];
        var message_dom = document.createElement('li');
        var content_dom = document.createTextNode('Received: ' + event.data);
        message_dom.appendChild(content_dom);
        messages_dom.appendChild(message_dom);
    };

    ws.onerror = function (event) {
        console.log(event);
    };

    ws.onclose = function (event) {
        console.log(event);
    };

    document.getElementById('send').onclick = function() {
//        fetch('/', {
//            method: 'POST',
//            headers: {
//                'Accept': 'application/json',
//                'Content-Type': 'application/json'
//            },
//            body: JSON.stringify ({
//                message: document.getElementsByName("message")[0].value,
//            }),
//        });
        ws.send(document.getElementsByName("message")[0].value);
        document.getElementsByName("message")[0].value = "";

    };
});
