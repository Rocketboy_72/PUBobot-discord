from quart import Quart
from quart_discord import DiscordOAuth2Session
from discord.ext import ipc
import client_config
import redis
import sentry_sdk
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

from .blueprints import auth, events, demo, websocket as websocket_bp

app = Quart(__name__)

if hasattr(client_config, "SENTRY_DSN"):
    sentry_sdk.init(dsn=client_config.SENTRY_DSN)
    SentryAsgiMiddleware(app.asgi_app)

app.config.from_object("web.server.config.Config")
app.redis_client = redis.from_url(app.config["REDIS_URL"])

# Required to access BOT resources.
# app.config["DISCORD_BOT_TOKEN"] = "..."

discord = DiscordOAuth2Session(app)
app.register_blueprint(auth.app)
app.register_blueprint(events.app)
app.register_blueprint(demo.app)
app.register_blueprint(websocket_bp.app)

app.ipc_client = ipc.Client(secret_key=client_config.IPC_SECRET)  # secret_key must be the same as your server
app.connected_websocket_clients = set()


# https://quart-discord.readthedocs.io/en/stable/introduction.html#basic-usage
# https://flask-caching.readthedocs.io/en/latest/

if __name__ == "__main__":
    app.run(debug=True)
