import discord
import argparse
from modules import config


class MyClient(discord.Client):

    def __init__(self, guild_id, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.guild_id = guild_id

    async def on_ready(self):
        print('discord client logged in')

        guild = await self.fetch_guild(self.guild_id)
        if guild is None:
            raise Exception("guild not found")
        print("guild:", guild.__dict__)
        await self.close()

    # async def on_error(self, event_method, *args, **kwargs):
    #     await self.close()


parser = argparse.ArgumentParser(description='adds role to given number of top players from channel')
parser.add_argument('guild_id', metavar='guild id', type=int)

arguments = parser.parse_args()

intents = discord.Intents.default()

client = MyClient(arguments.guild_id, intents=intents)
config.init()
client.run(config.cfg.DISCORD_TOKEN)
