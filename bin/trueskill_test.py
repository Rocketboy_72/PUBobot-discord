"""
run this from project root folder

pip install trueskill, optionally matplotlib

set environment variables:
CHANNEL_ID

optionally:
DB_FILE - input database file, default database.sqlite3
CSV_FILE - output csv filename, default trueskill.csv
AFTER - matches accounted after this timestamp, default all matches

to get timestamp for after:
from datetime import datetime
print(datetime.timestamp(datetime.fromisoformat('2021-01-22T18:30:00')))
"""

from modules import stats3
import trueskill
from collections import Counter
import csv
import os
import sys

try:
    import matplotlib.pyplot as plt
except ImportError:
    print("matplotlib not installed, not plotting alpha team win probability")
from datetime import datetime
import itertools
import math


def win_probability(team1, team2):
    """
    https://trueskill.org/#win-probability
    win probability of team1
    :param team1:
    :param team2:
    :return: float in range 0.0 - 1.0
    """
    delta_mu = sum(r.mu for r in team1) - sum(r.mu for r in team2)
    sum_sigma = sum(r.sigma ** 2 for r in itertools.chain(team1, team2))
    size = len(team1) + len(team2)
    denom = math.sqrt(size * (trueskill.BETA * trueskill.BETA) + sum_sigma)
    ts = trueskill.global_env()
    return ts.cdf(delta_mu / denom)


def run(channel_id, db_file=None, csv_file_name=None, after=None):
    if after is not None:
        after = int(after)
        print("matches after:", datetime.fromtimestamp(after))
    else:
        after = 0
    stats3.init(db_file)
    players = {}
    env = trueskill.setup(
        # draw_probability=0.2,
        # mu=1400,
        # sigma=350
    )
    env.make_as_global()
    counter = Counter()
    # alpha_win = Counter()
    alpha_win = []

    stats3.c.execute("SELECT * FROM pickups WHERE channel_id = ? and at > ? AND is_ranked ORDER BY at", (channel_id, after))
    # with open(f"{os.getenv('CSV_FILE_ALPHA')}" or'trueskill-alpha-win.csv', 'w') as alpha_win_file:
    for match in list(stats3.c.fetchall()):
        # print(list(match))
        stats3.c.execute("SELECT * FROM player_pickups WHERE pickup_id = ? order by at", (match['pickup_id'],))
        if match['winner_team'] == 'draw':
            scores = [0, 0]
        elif match['winner_team'] == 'alpha':
            scores = [0, 1]
        else:
            scores = [1, 0]

        alpha_team = []
        beta_team = []
        for match_player in stats3.c.fetchall():
            if match_player['user_id'] in players:
                player = players[match_player['user_id']]
            else:
                player = {
                    'id': match_player['user_id'],
                    'nick': match_player['user_name'],
                    'rating': trueskill.Rating(),
                    'wins': 0,
                    'loses': 0,
                    'draws': 0
                }
                players[match_player['user_id']] = player
            if match_player['team'] == 'alpha':
                alpha_team.append(player)
            else:
                beta_team.append(player)

            if match['winner_team'] == 'draw':
                player['draws'] += 1
            else:
                if match_player['is_winner']:
                    player['wins'] += 1
                else:
                    player['loses'] += 1
        if len(alpha_team) != len(beta_team) or len(alpha_team) == 0:
            print(f"match {match['pickup_id']} got wrong player count {len(alpha_team)}, {len(beta_team)}")
            continue
        teams = [[_p['rating'] for _p in alpha_team], [_p['rating'] for _p in beta_team]]
        # print('{:.1%} chance to draw'.format(quality(teams)))
        # print(winning_team, losing_team)
        # q = quality(teams)
        # alpha_win.update({f'{5 * round(100*win_probability(teams[0], teams[1]) / 5)}_alpha_win_chance': 1})
        alpha_win.append((match['at'] / (60 * 60 * 24), 100*win_probability(teams[0], teams[1])))
        (new_alpha_team), (new_beta_team) = trueskill.rate(teams, ranks=scores)
        # print(new_winning_team, new_losing_team)
        for _p, new_rating in zip(alpha_team + beta_team, new_alpha_team + new_beta_team):
            players[_p['id']]['rating'] = new_rating
        counter.update({'match': 1})

        if match['winner_team'] == 'draw':
            counter.update({'draw': 1})
        elif match['winner_team'] == 'alpha':
            counter.update({'alpha_win': 1})
        else:
            counter.update({'beta_win': 1})

    print(counter)
    # print(alpha_win)

    with open(csv_file_name or 'trueskill.csv', 'w', encoding='utf-8', newline='') as csv_file:
        fieldnames = ['id', 'nick', 'rating', 'mu-3sigma', 'mu', 'sigma', 'wins', 'loses', 'draws']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for _p_id in players:
            _p = players[_p_id]
            writer.writerow({
                'id': str(_p['id']),
                'nick': _p['nick'],
                'rating': round(env.expose(_p['rating']), 3),
                'mu-3sigma': round(_p['rating'].mu-3*_p['rating'].sigma, 3),
                'mu': round(_p['rating'].mu, 3),
                'sigma': round(_p['rating'].sigma, 3),
                'wins': _p['wins'],
                'loses': _p['loses'],
                'draws': _p['draws']
            })

    if 'matplotlib.pyplot' in sys.modules:
        fig, ax = plt.subplots(tight_layout=True)
        ax.hist([e[1] for e in alpha_win], bins=10)
        # ax.hist2d([e[0] for e in alpha_win], [e[1] for e in alpha_win], bins=[4, 20])
        plt.savefig(f"{csv_file_name}.png" or 'trueskill.png')
        plt.show()


if __name__ == "__main__":
    run(os.getenv('CHANNEL_ID'), os.getenv('DB_FILE'), os.getenv('CSV_FILE'), os.getenv('AFTER'))
